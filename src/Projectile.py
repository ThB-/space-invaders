import pygame


class Projectile(pygame.sprite.Sprite):

    def __init__(self,x,y,temps):
        super().__init__()
        self.temps = temps
        self.image = pygame.image.load("images/Missile.png")
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def getX(self):
        return self.rect.x
    def getY(self):
        return self.rect.y

    def update(self):
        i = int()
        i = 1
        self.rect.y -= (i * self.temps)


    def checkCollision(self, sprite1, sprite2):
        col = pygame.sprite.collide_rect(sprite1, sprite2)
        if col == True:
            return True
