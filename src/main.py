import pygame
from joueur import *
from Projectile import *
from Alien import *

fenetre = pygame.display.set_mode((800, 600))
image_Joueur = pygame.image.load("images/Vaisseau.png");
pygame.mixer.init()
son_tir = pygame.mixer.Sound("sounds/shoot.wav")
son_mort = pygame.mixer.Sound("sounds/invaderkilled.wav")
player = Joueur(image_Joueur,0,560)
gameover_image = pygame.image.load("images/gameover.jpg")
listeProjectiles = pygame.sprite.Group()
listeAliens = pygame.sprite.Group()
image_Projectile = pygame.image.load("images/Missile.png")
image_Alien = pygame.image.load("images/Alien1.png")
i = 0
pygame.init()

listeSprites = pygame.sprite.Group()
clock = pygame.time.Clock()
secondes = pygame.time.get_ticks() // 60
FRAMERATE = 30
continuer = True
dernierTir = 0
def creerAliens():

    for i in range(8):
        listeAliens.add(Alien(secondes * 4))


    for aliens in listeAliens:
        i +=50
        aliens.padding(i)


while continuer:
    clock.tick(FRAMERATE)
    secondess = pygame.time.get_ticks()
    dernierTir += secondes 
    fenetre.fill((0,0,0))
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player.reculer(50)
            if event.key == pygame.K_RIGHT:
                player.avancer(50)
            if event.key == pygame.K_SPACE:
                    print(dernierTir)
                    if dernierTir> 30:
                        son_tir.play()
                        listeProjectiles.add(Projectile(player.getX()+16,player.getY(),secondes * 4))
                        dernierTir = 0
        if event.type == pygame.QUIT:
                continuer = False
    if len(listeAliens) > 0:
        for aliens in listeAliens:
            aliens.update()
            fenetre.blit(aliens.image,(aliens.rect.x,aliens.rect.y))
        for project in listeProjectiles:
            project.update()
            fenetre.blit(project.image,(project.rect.x, project.rect.y))

        for project in listeProjectiles:
            for aliens in listeAliens:
                if project.checkCollision(project,aliens):
                    son_mort.play()
                    listeAliens.remove(aliens)
                    listeProjectiles.remove(project)
        fenetre.blit(image_Joueur, (player.getX(),player.getY()))
        pygame.display.update()
    else:
          creerAliens()

pygame.quit()
