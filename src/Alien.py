import pygame


class Alien(pygame.sprite.Sprite):
    def __init__(self,temps):
        super().__init__()
        self.image = pygame.image.load("images/Alien1.png")
        self.rect = self.image.get_rect()
        self.temps = temps
        self.rect.y = 50
        self.rect.x = 50
        self.tour = 0

    def update(self):
        i = int()
        i = 0.5
        if self.tour%2 == 0:
            if self.rect.x < 750:
                self.rect.x += i * self.temps
            else:
                self.tour = self.tour +1
                self.rect.y +=50
        if self.tour%2 == 1:
            if self.rect.x > 0:
                self.rect.x -= i * self.temps
                
            else:
                self.tour = self.tour +1
                self.rect.y += 50

            
    def padding(self,valeur):
        self.rect.x = valeur
