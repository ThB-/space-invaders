class Joueur:
    def __init__(self, image, posx,posy):
        self.image = image
        self.posx = posx
        self.posy = posy

    def reculer(self,valeur):
        if self.posx > 20:
            self.posx -= valeur
        else:
            print("Je suis hors de portée !")

    def avancer(self,valeur):
        if (self.posx < 660):
            self.posx +=valeur
        else:
            print("je suis hors de portée")

    def getX(self):
        return self.posx

    def getY(self):
        return self.posy
